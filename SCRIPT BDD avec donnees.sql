SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT;
SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS;
SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION;
SET NAMES utf8mb4;

CREATE DATABASE seriousbus_main;
USE seriousbus_main;

CREATE TABLE Utilisateur (
  `idUtilisateur` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NULL DEFAULT NULL,
  `prenom` varchar(25) NULL DEFAULT NULL,
  `mail` varchar(50) NULL DEFAULT NULL,
  `motdepasse` varchar(30) NULL DEFAULT NULL,
  `adresse` varchar(50) NULL DEFAULT NULL,
  `NPA` int(5) NULL DEFAULT NULL,
  `localite` varchar(30) NULL DEFAULT NULL,
  `numero` varchar(20) NULL DEFAULT NULL,
  `pays` varchar(30) NULL DEFAULT NULL,
  `confirmed` int(10) NULL DEFAULT 0,
  `fk_idStatut` int NOT NULL,
  PRIMARY KEY (`idUtilisateur`)
);
CREATE TABLE Statut (
  `idStatut` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(35) NULL DEFAULT NULL,
  PRIMARY KEY (`idStatut`)
);
ALTER TABLE Utilisateur ADD FOREIGN KEY (fk_idStatut) REFERENCES Statut (`idStatut`);

CREATE TABLE Article (
  `idArticle` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(25) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `prix` DECIMAL NOT NULL,
  `fk_idCategorie` INT NOT NULL,
  PRIMARY KEY (`idArticle`)
);
CREATE TABLE Categorie (
  `idCategorie` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idCategorie`)
);
ALTER TABLE Article ADD FOREIGN KEY (fk_idCategorie) REFERENCES Categorie (`idCategorie`);


-- AJOUT DE DONNEES POUR TESTER
INSERT INTO Statut VALUES ('1','Utilisateurs'),('2','Admin');
INSERT INTO Utilisateur VALUES ('1','Germann','Niels','niels.germann@cpnv.ch','1234','rue centrale 20','1450','Ste-Croix','0799471470','Suisse','1','2');
INSERT INTO Categorie VALUES ('1','Voitures'),('2','Avions et helicopteres'),('3','Yachts'),('4','Habits haute couture');

INSERT INTO Article (`idArticle`, `nom`, `description`, `prix`, `fk_idCategorie`) VALUES
(1, 'Ferrari LaFerrari', 'Nouvelle hypercar de chez ferrari, elle vous permettra d atteindre les 370km/h.', '3500000', 1),
(2, 'Porsche 918 Spyder', 'Dernière Hypercar de chez Porsche. Puissante, 4 roues motrices, avec 4 moteurs électriques d\'appoint', '1330000', 1),
(3, 'McLaren P1', 'Nouvelle hypercar de McLaren, possédant un énorme V12 ainsi qu\'un moteur électrique d\'appoint à l\'arrière.', '2222222', 1),
(4, 'F-35 Lockheed Martin', 'Un avion de chasse de chez Lockheed Martin, d\'une poussée de 178kN avec une masse d\'environ 20 tonnes avec armement et 13 tonnes à vide.', '212000000', 2),
(5, 'Eclipse', 'Le M/Y Eclipse est un yacht de luxe, construit par les chantiers Blohm & Voss de Hambourg en Allemagne en 2009.', '1000000000', 3),
(6, 'Bombardier Challenger 350', 'Un jet privé de taille modérée de bombardier qui peut accueillir 9 passagers et 3m cube de bagages. Il peut atteindre les 870km/h et possède un rayon d action de 6000km', '22000000', 2),
(7, 'GulfStream G550', 'Un jet privé de taille imposante qui vous permettra de faire jusqu à près de 12000km. Il peut accueillir jusqu à 14 passagers ainsi que 6.4m cube de bagages.', '42000000', 2),
(8, 'Rolls-Royce Wraith', 'Le célèbre coupé de chez Rolls-Royce avec un moteur V12 de 630ch ainsi que 800nM de couple. D une masse de 2400kg à vide ce véhicule est imposant et robuste.', '275000', 1),
(9, 'Kingdom 5KR', 'Le célèbre yacht luxueux du prince saoudien Al-Walid est désormais en vente sur SeriousBusiness ! Profitez de ces 85m de long avec jusqu à 30 personnes.', '101000000', 3),
(10, 'SS Delphine', 'Magnifique yacht à vapeur, entièrement réstauré. Sa longeur est d environ 80 mètres. Il peut accueillir un équipage d environ 25 personnes.', '20000000', 3),
(11, 'Skyacht One', 'Cet imposant jet privé est un basé sur un jet LINEAGE 1000E mais transformé par le designer Eddie Sotto.', '85000000', 3);



