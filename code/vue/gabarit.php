﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Serious Business</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/gabarit.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/formulaire.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="icon" type="image/x-icon" href="img/favicon.ico" />
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
</head>

<body>

<!--====================== NAVBAR MENU START===================-->


<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/logo.png" height="15.5px"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="index.php">Accueil</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Catégories<span class="caret"></span></a>
                    <ul class="dropdown-menu">
						<?php $categories = getCategories();
						while ($categorie = $categories->fetch(PDO::FETCH_ASSOC))
						{
							echo '<li><a href="index.php?action=vue_recherche&categorie='.utf8_encode($categorie['nom']).'">'.utf8_encode($categorie['nom']).'</a></li>';
						}
						?>
                    </ul>
                </li>
				<li><a href="index.php?action=vue_articles">Derniers articles</a></li>
                <li><a href="index.php?action=vue_contact">Nous contacter</a></li>
                <?php if (isset($_SESSION['mail']))
                { ?>
                    <li class="dropdown">
                        <a href="index.php?action=vue_sessionDestroy"><i class="fa fa-user" aria-hidden="true"></i> Se déconnecter</a>
                    </li>
                    <li class="dropdown">
                        <a href="index.php?action=vue_panier"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Panier</a>
                    </li>
                    <?php if ($_SESSION['type'] == 'admin')
                    {    ?>
                    <li class="dropdown">
                        <a href="index.php?action=vue_ajoutAnnonce"><i class="fa fa-database" aria-hidden="true"></i> Ajouter annonce</a>
                    </li>
                    <?php }
                }
                else
                { ?>
                    <li class="dropdown">
                        <a href="index.php?action=vue_signup"><i class="fa fa-user" aria-hidden="true"></i> Créer un compte</a>
                    </li>
                    <li class="dropdown">
                        <a href="index.php?action=vue_login"><i class="fa fa-user" aria-hidden="true"></i> Se connecter</a>
                    </li>
                    <?php
                } ?>
            </ul>
            <form class="navbar-form navbar-right" method="post" action="index.php?action=vue_recherche">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button class="btn btn-default-1" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                    <input type="text" class="form-control" placeholder="Rechercher ..." name="txtRecherche">
                </div>
            </form>

        </div>
    </div>
</nav>

<?php echo $contenu; ?>

<!--   FOOTER START================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4 class="title">Réseaux sociaux</h4>
                <p></p>
                <ul class="social-icon">
                    <a href="#" class="social"><i class="fa fa-facebook" aria-hidden="true">FACEBOOK</i></a><br>
                    <a href="#" class="social"><i class="fa fa-twitter" aria-hidden="true">TWITTER</i></a>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Mon compte</h4>
                <span class="acount-icon">
            <a href="#"><i class="fa fa-heart" aria-hidden="true"></i>Mes commandes</a>
            <a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i>Modifier mon compte</a>
          </span>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Méthodes de paiement</h4>
                <ul class="payment">
                    <li><a href="#"><i class="fa fa-cc-amex" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-credit-card" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-paypal" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                </ul>
    </div>
</footer>
