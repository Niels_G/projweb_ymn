<?php ob_start();
if (isset($_SESSION['mail']))
{
    if($_SESSION['type'] == 'admin')
    {
        echo '<h2>Bonjour administrateur '.$_SESSION['mail'].'</h2>';
    }
    if ($_SESSION['type'] == 'user')
    {
        echo '<h2>Bonjour '.$_SESSION['mail'].'<h2>';
    }
    echo '<h3>Vous êtes connecté.</h3>';
}
else
{
    echo '<h3>Il y a eu une erreur. Veuillez réessayer.</h3>';
}
?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";