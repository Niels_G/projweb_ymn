    <?php ob_start(); ?><br>
<div class="container">
    <div class="row">
        <?php foreach ($articles as $article)
        {
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".jpg")) { $type=".jpg";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".png")) { $type=".png";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".jpeg")) { $type=".jpeg";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".gif")) { $type=".gif";}
        ?>
        <div class="col-md-3 col-sm-6">
    		<span class="thumbnail">
				<div style="height: 200px; width: auto;">
					<img src="./userdata/imgAnnonces/<?=$article['nom'].$type?>" style="height: auto; width: 252px;">
				</div>
      			<h4><?=$article['nom'];?></h4>
      			<textarea style="resize:none;border:none;background-color: white;" rows="6" cols="29" disabled><?=utf8_encode($article['description']);?></textarea>
      			<hr class="line">
            <?php if (!isset($_SESSION['type'])){$_SESSION['type']="notConnected";}  ?>
                <?php if ($_SESSION['type'] == 'admin') {$prix = number_format( $article['prix'] , '0' , '.' , "'" ); echo '<p class="price">CHF '.$prix.'.-</p>'; } ?>
      			<div class="row">
      				<div class="col-md-6 col-sm-6">
      					<!--<p class="price"><?=number_format( $article['prix'] , '0' , '.' , "'" );?></p>-->
      				</div>
      				<div class="col-md-6 col-sm-6">
      					<form method="post" action="#">
                            <input type="text" hidden name="idArticle" value="<?=$article['idArticle']?>">
                            <?php if(($_SESSION['type']) != 'notConnected'){
                              if ($_SESSION['type'] != 'admin')
              								{
              									echo '<button type="submit" class="btn btn-success right">Ajouter au panier</button>';
              								}
              								else
              								{
              									echo '</form><a href="index.php?action=vue_modifAnnonce&ID='.$article['idArticle'].'"><button class="btn btn-success right">Modifier annonce</button></a>';
              								}
                            ;}
                            ?>


                        </form>
      				</div>
      			</div>
    		</span>
        </div>
        <?php } ?>
	</div>
</div>
<br>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
