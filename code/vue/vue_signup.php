<?php ob_start(); ?>

<?php
/**
 * Created by PhpStorm.
 * User: niels.germann
 * Date: 23.11.2017
 * Time: 08:43
 */
?>
<br>
<div class="main-login main-center">
    <form action="index.php?action=vue_signup_data" method="POST">
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Adresse e-mail</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="mail" placeholder="Entrez votre e-mail..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Prénom</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="prenom" placeholder="Entrez votre prénom..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Nom</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="nom" placeholder="Entrez votre nom..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Numéro de téléphone</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="numero" placeholder="Entrez votre numéro..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Adresse</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-home fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="adresse" placeholder="Entrez votre adresse..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Localité</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="localite" placeholder="Entrez votre localité..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">NPA</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="NPA" placeholder="Entrez votre NPA..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="cols-sm-2 control-label">Pays</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="pays" placeholder="Entrez votre pays..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="cols-sm-2 control-label">Mot de passe</label>
            <div class="cols-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                    <input type="password" class="form-control" name="motdepasse" placeholder="Entrez votre mot de passe..."/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="cols-sm-10">
                <div class="btn-lg btn-block login-button">
                    <input type="submit" class="form-control" value="Créer un compte"/>
                </div>
            </div>
        </div>
    </form>
</div>
<br>
<?php
$contenu = ob_get_clean();
require "gabarit.php";