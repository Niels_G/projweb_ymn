<?php ob_start(); ?>
<!--=================CAROUSELE START====================-->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>

    </ol>

    <!-- Wrapper for slides -->
    <center>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/laferrari.jpg" width="100%" height="100%">
                <div class="carousel-caption hidden-xs">
                    <p>La Ferrari</p>
                </div>
            </div>

            <div class="item">
                <img src="img/jetcarpack.jpg" width="100%" height="100%">
                <div class="carousel-caption hidden-xs">
                    <p>Pack Jet + Voiture</p>
                </div>
            </div>
            <div class="item">
                <img src="img/austinmartinDBC.jpg" width="100%" height="100%">
                <div class="carousel-caption hidden-xs">
                    <p>Austin Martin DBC</p>
                </div>
            </div>
            <div class="item">
                <img src="img/helico.jpg" width="100%" height="100%">
                <div class="carousel-caption hidden-xs">
                    <p>Voler armé</p>
                </div>
            </div>
        </div>
    </center>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Précédant</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Suivant</span>
    </a>
</div>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
