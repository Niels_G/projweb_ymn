<!--   FOOTER START================== -->
<br><footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4 class="title">Réseaux sociaux</h4>
                <p></p>
                <ul class="social-icon">
                    <a href="#" class="social"><i class="fa fa-facebook" aria-hidden="true">FACEBOOK</i></a>
                    <a href="#" class="social"><i class="fa fa-twitter" aria-hidden="true">TWITTER</i></a>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Mon compte</h4>
                <span class="acount-icon">
            <a href="#"><i class="fa fa-heart" aria-hidden="true"></i>Mes commandes</a>
            <a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i>Modifier mon compte</a>
          </span>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Méthodes de paiement</h4>
                <p>Pour l'instant, nous acceptons Paypal uniquement.</p>
                <ul class="payment">
                    <li><a href="#"><i class="fa fa-cc-amex" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-credit-card" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-paypal" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <hr>

        <div class="row text-center"> © 2017. Crée par Maxime Gaillard, Yann Apothéloz et Niels Germann</div>
    </div>
</footer>
