<?php
/**
 * Created by PhpStorm.
 * User: niels.germann
 * Date: 21.12.2017
 * Time: 13:47
 */
ob_start(); ?>
<br>
<center>
    <h2>Vos articles</h2><br>
        <div class="widget stacked widget-table action-table">
    <form method="post" action="index.php?action=vue_payement">
        <?php if (isset($lstPanier)) { ?>
            <table class="table table-striped table-bordered" border="1">
                <tr>
                    <th style="margin:10%;">Nom de l'article</th>
                    <th>Prix de l'article</th>
                    <th class="td-actions"></th>
                </tr>
                <?php
                    $totalPrix = 0;
                    $num=0;
                    foreach ($lstPanier as $article) {
                    $totalPrix = $totalPrix + $article['prix'];
                ?>
                <tr>
                    <td><input type="text" value="<?=$article['nom']?>" disabled></td>
                    <td><input type="text" value="<?=number_format($article['prix'],'0','.',"'")?>" disabled></td>
                    <td class="td-actions"><a href="index.php?action=vue_panier&num=<?=$num?>"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php $num++; }
                $_SESSION['prixPanier'] = $totalPrix;
                ?>
                <tr>
                    <td><div id="paypal-button-container"></div>
                        <?php Paypal(); 
                        if ($_SESSION['prixPanier']>= 9999999) {echo "Paiement paypal impossible au dessus de 9'999'999.-";}
                        ?></td>
                    <td>Total : CHF <?=number_format($totalPrix,'0','.',"'")?>.-</td>
                </tr>
            </table>
        <?php } else {echo '<h3>Vous n avez aucun article dans votre panier.</h3>';} ?>
    </form>
</center>
<br>
</div>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
