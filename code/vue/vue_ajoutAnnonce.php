<?php ob_start(); ?>

    <div class="container"  >
        <div class="row main">
            <div class="main-login main-center">
                <form action="index.php?action=vue_ajoutAnnonce_data" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Titre de l'annonce</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-align-justify" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="titreAnnonce" placeholder="Entrez le titre de votre annonce..." required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Description de l'annonce</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-sticky-note" aria-hidden="true"></i></span>
                                <textarea type="text" class="form-control" name="descriptionAnnonce" placeholder="Entrez la description de votre annonce..." required/></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Catégorie de l'annonce</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list" aria-hidden="true"></i></span>
                                <select name="categorie" style="width:170px;height:35px;">
                                    <?php $categories = getCategories();
                                    while ($categorie = $categories->fetch(PDO::FETCH_ASSOC))
                                    {
                                        if($_GET['categorie'] == $categorie['nom'] || $_POST['categorie'] == $categorie['nom'])
                                        {
                                            echo "<option value='".$categorie['nom']."' selected>".$categorie['nom']."</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='".$categorie['nom']."'>".$categorie['nom']."</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Prix de l'annonce</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="prixAnnonce" placeholder="Entrez le prix de votre annonce..." required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Images lié à l'annonce</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                                <input type="file" class="form-control" name="imgAnnonce" id="imgAnnonce" placeholder="Télécharger les images de votre annonce.." required/>
                            </div>
                        </div>
                    </div>
                </br>
                    <div class="form-group">
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <input type="submit" class="form-control" value="Valider l'annonce"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
$contenu = ob_get_clean();
require "gabarit.php";