<?php ob_start(); if (!empty($_POST)) {echo '<center><h2>Article modifié avec succès.</h2></center>';}?>
<br>
<div class="container"  >
	<div class="row main">
		<div class="main-login main-center">
			<form action="#" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name" class="cols-sm-2 control-label">Titre de l'annonce</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-align-justify" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="titreAnnonce" placeholder="Entrez le titre de votre annonce..." value="<?=$resultat['nom']?>" required/>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="cols-sm-2 control-label">Description de l'annonce</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-sticky-note" aria-hidden="true"></i></span>
							<textarea type="text" class="form-control" name="descriptionAnnonce" placeholder="Entrez la description de votre annonce..." required/><?=utf8_encode($resultat['description'])?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="cols-sm-2 control-label">Prix de l'annonce</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="prixAnnonce" placeholder="Entrez le prix de votre annonce..." value="<?=$resultat['prix']?>" required/>
						</div>
					</div>
				</div>
			</br>
				<div class="form-group">
					<div class="cols-sm-10">
						<div class="input-group">
							<input type="submit" class="form-control" value="Valider l'annonce"/>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<br>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
