<?php ob_start(); ?>
<?php if (!isset($_SESSION['type'])){$_SESSION['type']="notConnected";}  ?>
<br>
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="input-group">
                <div class="input-group-btn search-panel">
<center>
<form method="post" action="index.php?action=vue_recherche">
    <input type="text" size="100" name="txtRecherche" class="form-control" style="width:400px;" value="<?=@$_POST['txtRecherche']?>">
    <select class="btn btn-default dropdown-toggle" name="categorie">
		<option value="toutes" disabled selected>Par catégorie :</option>
        <?php $categories = getCategories();
        while ($categorie = $categories->fetch(PDO::FETCH_ASSOC))
        {
            if(utf8_encode($_GET['categorie']) == utf8_encode($categorie['nom']) || utf8_encode($_POST['categorie']) == utf8_encode($categorie['nom']))
            {
                echo "<option value='".utf8_encode($categorie['nom'])."' selected>".utf8_encode($categorie['nom'])."</option>";
            }
            else
            {
                echo "<option value='".utf8_encode($categorie['nom'])."'>".utf8_encode($categorie['nom'])."</option>";
            }
        }
        ?>
    </select>
    Trier par :
    <select class="btn btn-default dropdown-toggle" name="trier">
        <option value="toutes" disabled selected>Trier par :</option>
        <option value="dateDernier">Récent à Ancien</option>
        <option value="datePremier">Ancien à Récent</option>
    </select>
    <input type="submit" class="btn btn-default" value="Rechercher">



                </div>
            </div>
        </div>
    </div>
    </div>


<br>
<div class="container">
    <div class="row">
        <?php foreach ($articles as $article)
        {
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".jpg")) { $type=".jpg";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".png")) { $type=".png";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".jpeg")) { $type=".jpeg";}
            if (file_exists("./userdata/imgAnnonces/".$article['nom'].".gif")) { $type=".gif";}
            ?>
            <div class="col-md-3 col-sm-6">
				<span class="thumbnail">
					<div style="height: 200px; width: auto;">
						<img src="./userdata/imgAnnonces/<?=$article['nom'].$type?>" style="height: auto; width: 252px;">
					</div>
					<h4><?=$article['nom'];?></h4>
					<textarea style="resize:none;border:none;background-color: white;" rows="6" cols="29" disabled><?=utf8_encode($article['description']);?></textarea>
					<hr class="line">
                    <?php if ($_SESSION['type'] == 'admin') {$prix = number_format( $article['prix'] , '0' , '.' , "'" ); echo '<p class="price">CHF '.$prix.'.-</p>'; } ?>
					<div class="row">
						<div class="col-md-6 col-sm-6">
                        </div>
                        <?php if(($_SESSION['type']) != 'notConnected'){
                          if ($_SESSION['type'] != 'admin')
          								{
          									echo '<a class="btn btn-success right col-md-6 col-sm-6" href="index.php?action=vue_ajout_panier&idArticle='.$article['idArticle'].'">Ajouter au panier</a>';
          								}
          								else
          								{
          									echo '</form><a href="index.php?action=vue_modifAnnonce&ID='.$article['idArticle'].'"><button class="btn btn-success right">Modifier annonce</button></a>';
          								}
                        ;}
                        ?>

					</div>
				</span>
			</div>
        <?php } ?>
    </div>
</div>
<br>
</center>
</form>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
