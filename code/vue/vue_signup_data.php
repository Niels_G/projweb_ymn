<?php ob_start(); ?>

<?php
/**
 * Created by PhpStorm.
 * User: niels.germann
 * Date: 23.11.2017
 * Time: 08:44
 */?>
<br>
<?php if ($fail==1) {
    echo '<h3 style="color:red;">Erreur : e-mail déjà utilisé. Veuillez recommencez avec un mail différent.</h3>';
}
else
{
    echo '<h2>Un email de confirmation vous à été envoyé. </br> Vous devez confirmer votre compte afin de pouvoir vous connecter.</h2>';
}?>
<br>
<?php
$contenu = ob_get_clean();
require "gabarit.php";