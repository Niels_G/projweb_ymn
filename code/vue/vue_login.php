<?php ob_start(); ?>

    <br>
    <div class="main-login main-center">
        <form action="index.php?action=vue_login_data" method="POST">
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">E-mail</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="mail"   placeholder="Entrez votre e-mail..."/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="motdepasse"   placeholder="Entrez votre mot de passe..."/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Type de compte</label>
                    <div class="cols-sm-10">
                            <input type="radio" name="type" value="user" checked>Utilisateur &nbsp &nbsp &nbsp
                            <input type="radio" name="type" value="admin">Administrateur
                    </div>
                </div>
                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="btn-lg btn-block login-button">
                            <input type="submit" class="form-control" value="Se connecter"/>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
<br>

<?php
$contenu = ob_get_clean();
require "gabarit.php";