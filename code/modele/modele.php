﻿<?php
// FONCTIONS

//Fonction utilisée pour se connecter à la base de données contenant les articles, les catégories ainsi que les clients
function getBD()
{
  // connexion au serveur MySQL et à la BD jsaispas
  $connexion = new PDO('mysql:host=127.0.0.1; dbname=seriousbus_main', 'root', '');
  // permet d'avoir plus de détails sur les erreurs retournées
  $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $connexion;
}

//Fonction qui prend les données insérées dans la vue sign_up et les enregistres dans la base de données (créer compte client)
function createAccount()
{
    extract($_POST);

    $connexion = getBD();
    $queryTestuniqueEmail= "SELECT idUtilisateur FROM Utilisateur WHERE mail='".$mail."';";
    $resultatTest= $connexion->query($queryTestuniqueEmail);
    $resultatTestFetch = $resultatTest->fetch(PDO::FETCH_ASSOC);

    if (empty($resultatTestFetch['idUtilisateur']))
    {
        $query = "INSERT INTO Utilisateur (prenom,nom,mail,numero,adresse,localite,NPA,pays,motdepasse,fk_idStatut) VALUES ('".$prenom."','".$nom."','".$mail."','".$numero."','".$adresse."','".$localite."','".$NPA."','".$pays."','".$motdepasse."',1);";
        $resultat = $connexion->exec($query);
        return $resultat;
    }
    else
    {
        return;
    }
}

// Fonction qui vérifie les données insérées dans la vue login. Si le compte existe, se connecte ($_SESSION)
function connectUser()
{
    $connexion = getBD();
    extract($_POST);
    $query = "SELECT * FROM Utilisateur WHERE mail='".$mail."' AND motdepasse='".$motdepasse."' AND confirmed='1';";
    $resultat = $connexion->query($query);
    return $resultat;
}

// Fonction identique à connectUser, sauf que l'utilisateur doit être admin.
function connectAdmin()
{
    $connexion = getBD();
    extract($_POST);
    $query = "SELECT * FROM Utilisateur WHERE mail='".$mail."' AND motdepasse='".$motdepasse."' AND fk_idStatut='2' AND confirmed='1';";
    $resultat = $connexion->query($query);
    return $resultat;
}

// Fonction qui retourne la liste des articles pour la vue articles (derniers articles)
function chargerDerniersArticles()
{
	$connexion = getBD();
	$query = "SELECT * FROM Article ORDER BY idArticle DESC;";
	$resultat = $connexion->query($query);
	return $resultat;
}

// Fonction qui retourne la liste des catégories contenue dans la base de données
function getCategories()
{
	$connexion = getBD();
	$query = "SELECT idCategorie,nom FROM Categorie;";
	$resultat = $connexion->query($query);
	return $resultat;
}

// Fonction qui retourne la liste des articles de la vue recherche (grâce aux infos données, comme la catégorie ou une partie du nom de l'article)
function ChargerArticlesRecherche()
{
    $connexion = getBD();
    if (!empty($_POST['txtRecherche']) && isset($_POST['categorie']) && $_POST['categorie'] != 'Toutes')
    {
        $query = "SELECT idArticle,Article.nom,description,prix FROM Article INNER JOIN Categorie ON fk_idCategorie=idCategorie WHERE Article.nom LIKE '%".$_POST['txtRecherche']."%' AND Categorie.nom='".$_POST['categorie']."'";
    }
    else
    {
        if (isset($_POST['txtRecherche']))
        {
            $query = "SELECT idArticle,Article.nom,description,prix FROM Article WHERE nom LIKE '%".$_POST['txtRecherche']."%'";
        }
        if (isset($_GET['categorie']))
        {
            $query = "SELECT idArticle,Article.nom,description,prix FROM Article INNER JOIN Categorie ON fk_idCategorie=idCategorie WHERE Categorie.nom='".$_GET['categorie']."'";
        }
    }
    if (isset($_POST['trier']))
    {
        if ($_POST['trier'] == 'prixCroissant')
        { $query .= ' ORDER BY Article.prix;'; }

        if ($_POST['trier'] == 'prixDecroissant')
        { $query .= ' ORDER BY Article.prix DESC;'; }

        if ($_POST['trier'] == 'datePremier')
        { $query .= ' ORDER BY Article.idArticle'; }

        if ($_POST['trier'] == 'dateDernier')
        { $query .= ' ORDER BY Article.idArticle DESC'; }
    }
    $resultat = $connexion->query($query);
    return $resultat;
}

// Fonction qui permet d'ajouter un article dans la base de données, par rapport aux données insérées dans la vue ajoutAnnonce (Uniquement administrateur)
function ajoutAnnonceDB()
{
    $connexion = getBD();
    extract($_POST);
    $queryCategorie = "SELECT idCategorie FROM Categorie WHERE nom='".$categorie."'";
    $resultatCategorie = $connexion->query($queryCategorie);
    $idCat = $resultatCategorie->fetch(PDO::FETCH_ASSOC);
    ini_set('file_uploads','on');

    $imageFileType = strtolower(pathinfo(basename($_FILES["imgAnnonce"]["name"]),PATHINFO_EXTENSION));
    if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" )
    {
        move_uploaded_file($_FILES['imgAnnonce']['tmp_name'],"./userdata/imgAnnonces/".$titreAnnonce.".".$imageFileType);
    }

    $query = "INSERT INTO Article (nom,description,prix,fk_idCategorie) VALUES ('".$titreAnnonce."','".$descriptionAnnonce."','".$prixAnnonce."','".$idCat['idCategorie']."');";
    $resultat = $connexion->exec($query);
    return $resultat;
}

// Fonction qui retourne le nom et le prix de chaque article depuis la base de données, grâce à son ID contenue dans le panier ($_SESSION['panier'])
function chargerPanier()
{
    $connexion = getBD();
    $resultat = [];
    foreach ($_SESSION['panier'] as $idArticle)
    {
        $query = "SELECT Article.idArticle, Article.nom, Article.prix FROM Article WHERE idArticle='".$idArticle."'";
        $resultat[] = $connexion->query($query)->fetch();
    }
    return $resultat;
}

// Fonction qui permet d'activer le compte de l'utilisateur qui a confirmé son compte grâce au mail. Modifie la valeur de "confirmed" de l'utilisateur dans la base de données.
function confirmAccountDB()
{
    $connexion = getBD();

    $query = "UPDATE Utilisateur SET confirmed='1' WHERE mail ='".$_GET['mail']."'";
    $resultat = $connexion->exec($query);

    return $resultat;
}

// Fonction qui permet de supprimer un article de la base de données lorsque le payement a été efféctué.
function deleteArticle()
{
    foreach ($_SESSION['panier'] as $articleID)
    {
        $connexion = getBD();
        $query= "DELETE FROM Article WHERE idArticle=".$articleID.";";
        $resultat = $connexion->exec($query);
        unset($_SESSION['panier'][$articleID]);
        unset($_SESSION['panier']);
        $_SESSION['prixPanier'] = 0;
        // "Supprimer l'img de l'annonce.
    }
}

// Fonction qui permet de valider le payement grâce aux API's de Paypal. Version Developper, Sandbox.
function Paypal(){
    ?><script>
        paypal.Button.render({

            env: 'sandbox', // sandbox | production

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox:    'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
                production: '<insert production client id>'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: <?php echo $_SESSION['prixPanier']; ?>, currency: 'CHF' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    window.alert('Paiement Réussi!');
                    document.location.href="index.php?action=vue_deleteArticlePage";
                });
            }

        }, '#paypal-button-container');

    </script><?php
}

function chargerAnnonce($idArticle)
{
	$connexion = getBD();
	$query = "SELECT * FROM Article WHERE idArticle=".$idArticle.";";

	$resultat = $connexion->query($query);
	$resultatArticle = $resultat->fetch(PDO::FETCH_ASSOC);
	return $resultatArticle;
}

function modifierAnnonceDB()
{
	$connexion=getBD();
	$query="UPDATE Article SET nom='".utf8_decode($_POST['titreAnnonce'])."', description='".utf8_decode($_POST['descriptionAnnonce'])."', prix=".$_POST['prixAnnonce']." WHERE idArticle=".$_GET['ID'].";";
	$resultat = $connexion->exec($query);

	return $resultat;
}
