<?php
session_set_cookie_params(1800); // La session aura une durée de vie de 20 min puis elle sera détruite automatiquement
ini_set('session.gc_maxlifetime','1800');
ini_set('session.cookie_lifetime','1800');
session_start();

// index.php
// date de création : 18/05/16

require 'controleur/controleur.php';

if (isset($_GET['action']))
{
    // sélection de l'action à réaliser
    $action = $_GET['action'];

    switch ($action)
    {
        case 'vue_accueil':
            accueil(); // demande l'affichage du login
            break;
        case 'vue_login':
            login();
            break;
        case 'vue_login_data':
            login_data();
            break;
        case 'vue_signup':
            signup();
            break;
        case 'vue_signup_data':
            signup_data();
            break;
        case 'vue_logoff':
            logoff();
            break;
        case 'vue_sessionDestroy':
            sessionDestroy();
            break;
        case 'vue_contact':
			     contact();
			       break;
		    case 'vue_articles':
			     derniersArticles();
		        break;
        case 'vue_recherche':
            rechercherArticles();
            break;
        case 'vue_ajoutAnnonce':
            ajoutAnnonce();
            break;
        case 'vue_ajoutAnnonce_data':
            ajoutAnnoncedata();
            break;
        case 'vue_panier':
            afficherPanier();
            break;
        case 'vue_confirm':
            confirmAccount();
            break;
		case 'vue_ajout_panier':
			ajoutPanierData();
			break;
      case 'vue_deleteArticlePage':
  			deleteArticlePage();
  			break;
        case 'vue_modifAnnonce':
    			modifierAnnonce();
    			break;

		default:
            throw new Exception("action non valide");
    }

}
else
{
    accueil(); // affiche par défaut la page d'accueil
}
?>
