<?php
// controleur.php
// Date de création : 18.05.16

require 'modele/modele.php';
// Affichage de la page d'accueil


/**
 * Name: accueil
 * Description: Appel la vue de l'accueil
 */
function accueil()
{
    require 'vue/vue_accueil.php';
}

/**
 * Name: login
 * Description: Appel la vue login
 */
function login()
{
    require 'vue/vue_login.php';
}

/**
 * Name: login_data
 * Description: Cette fonction test au moment de la connexion si le compte est un compte "user" ou "administrateur" et appel la vue login_data
 */
function login_data()
{
    if ($_POST['type'] == 'user')
    {
        $resultat = connectUser();
        $donnee = $resultat->fetch();
        if (!empty($donnee))
        {
            $_SESSION['mail'] = $_POST['mail'];
            $_SESSION['type'] = $_POST['type'];
        }
        require 'vue/vue_login_data.php';
    }
    if ($_POST['type'] == 'admin')
    {
        $resultat = connectAdmin();
        $donnee = $resultat->fetch();
        if (!empty($donnee))
        {
            $_SESSION['mail'] = $_POST['mail'];
            $_SESSION['type'] = $_POST['type'];
        }
        require 'vue/vue_login_data.php';
    }
}

/**
 * Name: signup
 * Description: appel la vue signup
 */
function signup()
{
    require 'vue/vue_signup.php';
}

/**
 * Name: signup_data
 * Description: Appel la fonction createAccount, si la requête SQL est correctement exécutée, envoi un mail de confirmation
 */
function signup_data()
{
    $res = createAccount();
    if (empty($res))
    {
        require 'vue/vue_signup_data_fail.php';
    }
    else
    {
        ini_set('SMTP','mail.cpnv.ch');
        mail($_POST['mail'],'Serious Business - Contact',"Welcome to Serious Business!\n\nPlease Confirm your account by clicking on the link bellow. \nhttp://www.seriousbusiness.mycpnv.ch/index.php?action=vue_confirm&yes=1&mail=".$_POST['mail']."\n PLEASE DO NOT REPLY TO THIS MAIL.","From: contact@seriousbusiness.ch");

        require 'vue/vue_signup_data.php';
    }
}

/**
 * Name: confirmAccount
 * Description: Fonction permettant de vérifier l'adresse mail après la création de compte
 */
function confirmAccount()
{
    confirmAccountDB();
    require 'vue/vue_confirm.php';
}

/**
 * Name: logoff
 * Description: fonction qui permet à l'utilisateur ou l'administrateur de se déconnecter du site, appel la vue logoff
 */
function logoff()
{
    require 'vue/vue_logoff.php';
}
function sessionDestroy()
{

    session_destroy();
    require 'vue/vue_sessionDestroy.php';
}

// Permet d'envoyer un mail aux développeurs, pour leur soumettre des critiques par rapport au site.
function contact()
{
    require 'vue/vue_contact.php';
}

/**
 * Name: derniersArticles
 * Description: Appel la fonction chargerDerniersArticles, vérifie ensuite l'exécution de la fonction  puis appel la page articles.
 */
function derniersArticles()
{
    $articles = chargerDerniersArticles();
    if (isset($_POST['idArticle']))
    {
        $_SESSION['panier'][] = $_POST['idArticle'];
    }
    require 'vue/vue_articles.php';
}

/**
 * Name: rechercherArticles
 * Description: Fonction permettant la recherche
 */
function rechercherArticles()
{
    if (isset($_GET['idArticle']))
    {
        $_SESSION['panier'][] = $_GET['idArticle'];
    }
    else
    {
        if (isset($_POST['txtRecherche']) || !empty($_POST['categorie']) || !empty($_GET['categorie']))
        {
            $articles = ChargerArticlesRecherche();

        }
    }
    require 'vue/vue_recherche.php';
}

/**
 * Name: ajoutAnnonce
 * Description: Fonction permettant de de vérifier le statut du compte avant l'ajout de l'annonce
 */
function ajoutAnnonce()
{
    if ($_SESSION['type'] == 'admin')
    {
        require 'vue/vue_ajoutAnnonce.php';
    }
    else
    {
        echo 'Vous n avez pas les droits pour accéder à cette page.';
    }
}

/**
 * Name: ajoutAnnoncedata
 * Description: Fonction permettant de rajouter une annonce, puis appel la page ajoutAnnonce
 */
function ajoutAnnoncedata()
{
    $resultat = ajoutAnnonceDB();
    require 'vue/vue_ajoutAnnonce_data.php';
}

/**
 * Name: afficherPanier
 * Description: Affiche le panier en fonction de si il est vide, si il est confirmé, etc.
 */
function afficherPanier()
{
    if (isset($_GET['num']))
    {   if (isset($_SESSION['panier']))
    {
        $_SESSION['panier'] = array_values($_SESSION['panier']);
    }
        unset($_SESSION['panier'][$_GET['num']]);
        if (empty($_SESSION['panier']))
        {
            unset($_SESSION['panier']);
        }
    }
    if (isset($_SESSION['panier']))
    {
        $lstPanier = chargerPanier();
    }
    require 'vue/vue_panier.php';
}

/**
 * Name: ajoutPanierData
 * Description: Fonction permettant de rajouter des produits au panier
 */
function ajoutPanierData()
{
    $_SESSION['panier'][] = $_GET['idArticle'];
    require 'vue/vue_ajout_panier.php';
}
function deleteArticlePage()
{
    deleteArticle();
    require 'vue/vue_deleteArticle.php';
}


function modifierAnnonce()
{
	$resultat = chargerAnnonce($_GET['ID']);
	if (!empty($_POST))
	{
		modifierAnnonceDB($_POST);
	}

	require 'vue/vue_modifAnnonce.php';
}
